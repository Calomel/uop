#include <common/Suite.hpp>

#include <uop/Parser.hpp>
#include <uop/OptionBool.hpp>
#include <uop/OptionString.hpp>

void case_1(Suite& suite)
{
	using namespace uop;

	Parser parser;

	OptionBool ob1("verbose", "v", false);
	OptionBool ob2("experimental", "exp", false);

	OptionString os1("solver", "s", std::string());
	OptionString os2("language", "lang", "la");

	parser.addOption(&ob1);
	parser.addOption(&ob2);
	parser.addOption(&os1);
	parser.addOption(&os2);

	char const* argv[] = {
		"--verbose",
		"-exp=0",
		"--solver=circular",
	};

	parser.parse(3, argv);

	suite.assertEqual(ob1.value(), true,  "ob1.v");
	suite.assertEqual(ob1.isHit(), true,  "ob1.t");
	suite.assertEqual(ob2.value(), false, "ob2.v");
	suite.assertEqual(ob2.isHit(), true,  "ob2.t");

	suite.assertEqual(os1.value(), "circular", "os1.v");
	suite.assertEqual(os1.isHit(), true,       "os1.t");
	suite.assertEqual(os2.value(), "la",       "os2.v");
	suite.assertEqual(os2.isHit(), false,      "os2.t");

	suite.assert(parser.success(), "Success");
}

int main()
{
	Suite suite;

	case_1(suite);

	return suite.accumulate();
}
