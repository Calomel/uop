#ifndef __UOP_CaptureMap_hpp
#define __UOP_CaptureMap_hpp

#include <string>
#include <unordered_map>

namespace uop
{

class Option;

class CaptureMap
{
public:
	CaptureMap& add(std::string const& key, Option* opt)
	{
		map_[key] = opt;
		return *this;
	}
	bool exists(std::string const& key)
	{
		return map_.count(key);
	}
	Option* operator[](std::string const& key)
	{
		return map_[key];
	}
private:
	std::unordered_map<std::string, Option*> map_;
};
/**
 * Map from string keys to possible parsers
 */

} // namespace uop

#endif // !__UOP_CaptureMap_hpp
