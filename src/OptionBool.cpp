#include "OptionBool.hpp"

#include "OptionFlag.cpp.in"

namespace uop
{

bool
OptionBool::parse(int* const step, int argc, char const* argv[])
{
	hit_ = true;
	*step = 1;

	if (argv[0][0] == '\0')
	{
		// Empty array
		value_ = true;
	}
	else if (argv[0][0] == '=')
	{
		switch (argv[0][1])
		{
		case '0':
			value_ = false;
			break;
		case '1':
			value_ = true;
			break;
		default:
			return -1;
		}
	}
	else
		return false;

	return true;
}

} // namespace uop
