#include "OptionString.hpp"

#include "OptionFlag.cpp.in"

namespace uop
{

bool
OptionString::parse(int* const step, int argc, char const* argv[])
{
	hit_ = true;
	*step = 1;

	if (argv[0][0] == '\0')
	{
		// No key found
		return false;
	}
	else if (argv[0][0] == '=')
	{
		value_ = std::string(argv[0] + 1);
		return true;
	}
	else
		return false;
}

} // namespace uop
