#ifndef __UOP_Option_hpp
#define __UOP_Option_hpp

#include <string>

namespace uop
{

class CaptureMap;

class Option
{
public:
	/**
	 * Add the keys of this option to the map, so a parser that encounters the
	 * aforementioned key can call this option.
	 */
	virtual void addCapture(CaptureMap&) = 0;

	/**
	 * @brief Parse the argc argv[] array.
	 *
	 * Pre-conditions:
	 *   argv[0] is matched with one of the captures, with the captured part
	 *   discarded.
	 *
	 *   e.g. --tool=memcheck will be matched with --tool,
	 *        and argv[0] will be modified to =memcheck
	 *
	 *        --tool          will also be matched with --tool,
	 *        and argv[0] will be a empty string
	 *   argc > 0
	 *
	 * @param[out] step Number of arguments consumed. Must be > 0.
	 *                  Note that even if the parse is unsuccessful, this field
	 *                  needs to be filled with a meaningful value.
	 * @param[in] argc Size of array argv
	 * @param[in] argv Arg contents
	 * @return True if successful.
	 */
	virtual bool parse(int* const step, int argc, char const* argv[]) = 0;
};

} // namespace uop

#endif // !__UOP_Option_hpp
